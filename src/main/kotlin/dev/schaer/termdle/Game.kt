package dev.schaer.termdle

private const val ESC = "\u001B"
private const val BG_YELLOW = "$ESC[103m"
private const val BG_GREEN = "$ESC[102m"
private const val FG_BLACK = "$ESC[0;90m"
private const val RESET = "$ESC[0m"

class Game(wordToGuessIn: String) {
    private val wordToGuessStr: String = wordToGuessIn.uppercase()
    private val wordLen: Int = wordToGuessIn.length
    val wordToGuess: CharArray = wordToGuessStr.toCharArray()

    fun play() {
        var guess = 1
        print("Guess the $wordLen letter word:")
        while(true) {
            print("\n$guess> ${"_".repeat(wordLen)}$ESC[${wordLen}D")
            val input = readLine()
            if(input?.let { printInput(it) } == true) {
                println("\nHorray!")
                break
            }
            guess++
        }
    }

    private fun printInput(inStr: String): Boolean {
        val input = inStr.uppercase().padEnd(wordLen, ' ')
        print("$ESC[F$ESC[3C")
        for(i in wordToGuess.indices) {
            val inChar = input[i]
            if(wordToGuess[i] == inChar) {
                printGreen(inChar.toString())
            } else if(wordToGuess.contains(inChar)) {
                printYellow(inChar.toString())
            } else {
                print(inChar.toString())
            }
        }
        return input == wordToGuessStr
    }

    private fun printYellow(arg: String) {
        print("$FG_BLACK$BG_YELLOW$arg$RESET")
    }

    private fun printGreen(arg: String) {
        print("$FG_BLACK$BG_GREEN$arg$RESET")
    }
}
