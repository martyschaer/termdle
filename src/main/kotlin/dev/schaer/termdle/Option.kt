package dev.schaer.termdle

enum class Option {
    EXTENDED,
    RANDOM,
    HELP
}

fun parseOption(arg: String): Option {
    return when(arg) {
        "-r" -> Option.RANDOM
        "-e" -> Option.EXTENDED
        "-h" -> Option.HELP
        else -> error("Unrecognized option $arg")
    }
}