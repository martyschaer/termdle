package dev.schaer.termdle

import java.nio.charset.StandardCharsets
import java.time.ZoneId
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter.BASIC_ISO_DATE
import kotlin.random.Random

private const val DEFAULT_WORD_LENGTH = 5

fun main(args: Array<String>) {
    App().run(argParse(args))
}

private fun argParse(args: Array<String>): Set<Option> {
    return args.map { parseOption(it) }.toSet()
}

class App() {
    fun run(options: Set<Option>) {
        if(options.contains(Option.HELP)) {
            printHelp()
            return
        }

        val words = App::class.java.getResource("/words.txt")
            ?.readText(StandardCharsets.UTF_8)
            ?.lines() ?: error("Unable to load word list.")


        val rng = getRng(options)
        val isExtended = options.contains(Option.EXTENDED)

        val word = words
            .filter { it.isNotBlank() }
            .filter { isExtended || it.length == DEFAULT_WORD_LENGTH }
            .random(rng)
        Game(word).play()
    }

    private fun getRng(options: Set<Option>): Random {
        // if -r is given, pick truly randomly
        if (options.contains(Option.RANDOM)) {
            return Random
        }
        // YYYYMMDD at UTC as the seed to pick a new word every day
        val seed = ZonedDateTime.now(ZoneId.of("Z"))
            .format(BASIC_ISO_DATE).substring(0, 8).toInt()
        return Random(seed)
    }

    private fun printHelp() {
        println("""
            termdle - a wordle inspired guessing game
            
            Guess a word of the given length by typing your guess and hitting enter.
            Letters highlighted in yellow appear in the word, but not at the position you've entered.
            Letters highlighted in green appear in the word at the position you've entered.
            Letters that aren't highlighted do not appear in the word.
            
            Getting it in 3 guesses is excellent, 4 is good.
            Anything less than those is probably luck.
            It should be possible to guess words within 6 attempts,
            but you're not limited.
            
            Options:
            
             -r     Truly random, picks a new word from the list every time.
                    If not given, chooses a new word every 24h.
             
             -e     Extended, picks words of any length between 4 and 15 (inclusive).
                    If not given, only chooses 5 letter words.
                    
             -h     Display this information.
             
             Source available on GitLab: https://gitlab.com/martyschaer/termdle
             
             Hacked with ♥ by schaer.dev
             
             
        """.trimIndent())
    }
}
