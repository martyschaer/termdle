# termdle
A worlde-inspired guessing game for your terminal, written in Kotlin.

## screenshots
![](assets/5-letter-word.png)

![](assets/10-letter-word.png)
